import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service';
import { Router } from '@angular/router';
import { LANGUAGES } from 'src/app/shared/enum';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'emo-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public languages = LANGUAGES;

  constructor(
    private readonly route: Router,
    private readonly authService: AuthenticationService,
    private readonly translateService: TranslateService) { }

  ngOnInit() {  }

  public get isLoggedIn(): boolean {
    return this.authService.isLoggedIn();
  }

  public logOut(): void {
    this.authService.logOut();
    this.route.navigate(['/']);
  }

  public languageChange(lang: any): void {
    this.translateService.use(lang.target.value);
  }

  public get defaultLang(): string {
    return this.translateService.getDefaultLang();
  }

  public isDefaultLang(lang: any): boolean {
    return lang === this.defaultLang;
  }
}
