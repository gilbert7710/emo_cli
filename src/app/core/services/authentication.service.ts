import { Injectable } from '@angular/core';
import { LocalStorageService } from './local-storage.service';
import { CONFIG } from 'src/app/config';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private readonly localStorageService: LocalStorageService) { }

  public login(data: any): boolean {
    const { email, password } = CONFIG.adminUser;

    if (data.email === email && data.password === password) {
      this.localStorageService.set('authUser', data);
      return true;
    }

    return false;
  }

  public isLoggedIn(): boolean {
    if (!this.localStorageService.get('authUser')) {
      return false;
    }

    return true;
  }

  public getAuhtData(): any {
    return this.localStorageService.get('authUser');
  }

  public logOut(): void {
    this.localStorageService.delete('authUser');
  }
}
