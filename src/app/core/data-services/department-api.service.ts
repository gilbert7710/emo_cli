import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CONFIG } from 'src/app/config';
import { map, catchError } from 'rxjs/operators';
import { HandleErrorService } from '../services/handle-error.service';

@Injectable({
  providedIn: 'root'
})
export class DepartmentApiService {

  constructor(private http: HttpClient, private readonly handleError: HandleErrorService) { }

  public getAll(pQuery: any): Observable<any> {
    return this.http.get(`${CONFIG.baseURL}/department`, { params: pQuery })
      .pipe(map(data => data), catchError(this.handleError.handleError<any>('getAllDepartment')));
  }

  public add(pDepartment: any): Observable<any> {
    return this.http.post(`${CONFIG.baseURL}/department`, pDepartment)
      .pipe(catchError(this.handleError.handleError<any>('addDepartment')));
  }

  public update(id: number, pDepartment: any): Observable<any> {
    return this.http.put(`${CONFIG.baseURL}/department/${id}`, pDepartment)
      .pipe(catchError(this.handleError.handleError<any>('updateDepartment')));
  }

  public delete(id: number): Observable<any> {
    return this.http.delete(`${CONFIG.baseURL}/department/${id}`)
      .pipe(catchError(this.handleError.handleError<any>('deleteDepartment')));
  }
}
