import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CONFIG } from 'src/app/config';
import { map, catchError } from 'rxjs/operators';
import { HandleErrorService } from '../services/handle-error.service';

@Injectable({
  providedIn: 'root'
})
export class WarehouseApiService {

  constructor(private http: HttpClient, private readonly handleError: HandleErrorService) { }

  public getAll(pQuery: any): Observable<any> {
    return this.http.get(`${CONFIG.baseURL}/warehouse`, { params: pQuery })
      .pipe(map(data => data), catchError(this.handleError.handleError<any>('getAllWarehouse')));
  }

  public add(pWarehouse: any): Observable<any> {
    return this.http.post(`${CONFIG.baseURL}/warehouse`, pWarehouse)
      .pipe(catchError(this.handleError.handleError<any>('addWarehouse')));
  }

  public update(id: number, pWarehouse: any): Observable<any> {
    return this.http.put(`${CONFIG.baseURL}/warehouse/${id}`, pWarehouse)
      .pipe(catchError(this.handleError.handleError<any>('updateWarehouse')));
  }

  public delete(id: number): Observable<any> {
    return this.http.delete(`${CONFIG.baseURL}/warehouse/${id}`)
      .pipe(catchError(this.handleError.handleError<any>('deleteWarehouse')));
  }
}
