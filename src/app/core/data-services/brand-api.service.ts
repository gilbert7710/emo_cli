import { HandleErrorService } from './../services/handle-error.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CONFIG } from 'src/app/config';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BrandApiService {

  constructor(private http: HttpClient, private readonly handleError: HandleErrorService) { }

  public getAll(pQuery: any): Observable<any> {
    return this.http.get(`${CONFIG.baseURL}/brand`, { params: pQuery })
      .pipe(map(data => data), catchError(this.handleError.handleError<any>('getAllBrand')));
  }

  public add(pBrand: any): Observable<any> {
    return this.http.post(`${CONFIG.baseURL}/brand`, pBrand)
      .pipe(catchError(this.handleError.handleError<any>('addBrand')));
  }

  public update(id: number, pBrand: any): Observable<any> {
    return this.http.put(`${CONFIG.baseURL}/brand/${id}`, pBrand)
      .pipe(catchError(this.handleError.handleError<any>('updateBrand')));
  }

  public delete(id: number): Observable<any> {
    return this.http.delete(`${CONFIG.baseURL}/brand/${id}`)
      .pipe(catchError(this.handleError.handleError<any>('deleteBrand')));
  }
}
