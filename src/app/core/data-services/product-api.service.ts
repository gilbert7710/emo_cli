import { CONFIG } from '../../config/index';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { HandleErrorService } from '../services/handle-error.service';

@Injectable({
  providedIn: 'root'
})
export class ProductApiService {
  private query: any = {};

  constructor(private http: HttpClient, private readonly handleError: HandleErrorService) { }

  public getAll(): Observable<any> {
    this.query = { pagesize: 1000 };
    return this.http.get(`${CONFIG.baseURL}/product`, { params: this.query })
      .pipe(map(data => data), catchError(this.handleError.handleError<any>('getAllProduct')));
  }

  public add(pProduct: any): Observable<any> {
    return this.http.post(`${CONFIG.baseURL}/product`, pProduct)
      .pipe(catchError(this.handleError.handleError<any>('addProduct')));
  }

  public update(id: number, pProduct: any): Observable<any> {
    return this.http.put(`${CONFIG.baseURL}/product/${id}`, pProduct)
      .pipe(catchError(this.handleError.handleError<any>('updateProduct')));
  }

  public delete(id: number): Observable<any> {
    return this.http.delete(`${CONFIG.baseURL}/product/${id}`)
      .pipe(catchError(this.handleError.handleError<any>('deleteProduct')));
  }
}
