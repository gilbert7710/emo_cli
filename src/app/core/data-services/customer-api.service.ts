import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { CONFIG } from 'src/app/config';
import { HandleErrorService } from '../services/handle-error.service';

@Injectable({
  providedIn: 'root'
})
export class CustomerApiService {

  constructor(private http: HttpClient, private readonly handleError: HandleErrorService) { }

  public getAll(): Observable<any> {
    return this.http.get(`${CONFIG.baseURL}/customer`)
      .pipe(map(data => data), catchError(this.handleError.handleError<any>('getAllCustomer')));
  }

  public add(pCustomer: any): Observable<any> {
    return this.http.post(`${CONFIG.baseURL}/customer`, pCustomer)
      .pipe(catchError(this.handleError.handleError<any>('addCustomer')));
  }

  public update(id: number, pCustomer: any): Observable<any> {
    return this.http.put(`${CONFIG.baseURL}/customer/${id}`, pCustomer)
      .pipe(catchError(this.handleError.handleError<any>('updateCustomer')));
  }

  public delete(id: number): Observable<any> {
    return this.http.delete(`${CONFIG.baseURL}/product/${id}`)
      .pipe(catchError(this.handleError.handleError<any>('deleteCustomer')));
  }
}
