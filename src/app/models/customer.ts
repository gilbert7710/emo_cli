export class Customer {
  type: BigInteger;
  code: string;
  name: string;
}
