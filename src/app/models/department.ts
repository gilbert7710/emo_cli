export class Department {
  id: string;
  code: string;
  desc: string;
  status: number;
}
