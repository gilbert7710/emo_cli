export class Product {
  code: string;
  name: string;
  typeProduct: number;
  brandId: number;
  measure: number;
  status: number;
}
