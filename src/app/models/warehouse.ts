export class Warehouse {
    code: string;
    name: string;
    phone1: string;
    phone2: string;
    address: string;
    status: number;
  }
  