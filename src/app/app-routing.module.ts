import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { AuthGuard } from './core/guards/auth.guard';


const routes: Routes = [
  // {path: '', pathMatch: 'full', redirectTo: 'dashboard'},
  {
    path: '',
    loadChildren: './modules/authentication/authentication.module#AuthenticationModule'
  },
  {
    path: 'home', canActivate: [AuthGuard],
    loadChildren: './modules/customers/customers.module#CustomersModule'
  },
  {
    path: 'customers', canActivate: [AuthGuard],
    loadChildren: './modules/customers/customers.module#CustomersModule'
  },
  {
    path: 'departments', canActivate: [AuthGuard],
    loadChildren: './modules/departments/departments.module#DepartmentsModule'
  },
  {
    path: 'brands', canActivate: [AuthGuard],
    loadChildren: './modules/brands/brands.module#BrandsModule'
  },
  {
    path: 'warehouses', canActivate: [AuthGuard],
    loadChildren: './modules/warehouses/warehouses.module#WarehousesModule'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    preloadingStrategy: PreloadAllModules,
    onSameUrlNavigation: 'reload'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
