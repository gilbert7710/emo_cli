import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DepartmentsRoutingModule } from './departments-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DepartmentModalComponent } from './department-modal/department-modal.component';
import { ParentComponent } from './parent/parent.component';
import { ListComponent } from './list/list.component';


@NgModule({
  declarations: [ParentComponent, ListComponent, DepartmentModalComponent],
  imports: [
    CommonModule,
    DepartmentsRoutingModule,
    SharedModule,
    NgbModule
  ],
  entryComponents: [DepartmentModalComponent]
})
export class DepartmentsModule { }
