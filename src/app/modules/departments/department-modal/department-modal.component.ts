import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DepartmentApiService } from 'src/app/core/data-services/department-api.service';

@Component({
  selector: 'emo-department-modal',
  templateUrl: './department-modal.component.html',
  styleUrls: ['./department-modal.component.scss']
})
export class DepartmentModalComponent implements OnInit {

  @Input()
  public isEdit: boolean;
  @Input()
  public department: any;
  @Output()
  public readonly successs: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output()
  public createdDepartment: EventEmitter<any> = new EventEmitter<any>();
  @Output()
  public editedDepartment: EventEmitter<boolean> = new EventEmitter<boolean>();

  public form: FormGroup;

  constructor(
    private readonly fb: FormBuilder,
    private readonly departmentService: DepartmentApiService,
    private readonly modalInstance: NgbActiveModal) { }

  ngOnInit() {
    this.initForm();
  }

  private initForm(): void {
    this.form = this.fb.group({
      code: ['', Validators.required],
      desc: ['', Validators.required],
      status: true
    });

    if (this.isEdit) {
      this.form.patchValue(this.department);
    }
  }

  public onSubmit(): void {
    if (this.isEdit) {
      this.departmentService
        .update(this.department.id, this.form.value)
        .subscribe(
          () => {
            this.successs.emit(true);
            this.dismiss();
          },
          error => {
            console.log(error);
          }
        );
    } else {
      // const payload: any = {};
      this.createdDepartment.emit(this.form.value);
    }
  }

  public dismiss(): void {
    this.modalInstance.dismiss();
  }

}
