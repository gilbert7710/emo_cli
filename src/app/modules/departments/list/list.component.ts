import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DepartmentApiService } from 'src/app/core/data-services/department-api.service';
import { DepartmentModalComponent } from '../department-modal/department-modal.component';
import Swal from 'sweetalert2';

@Component({
  selector: 'emo-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  public departmentList: any[];

  @Input()
  public filterBy: any;
  @Input()
  public department: any;
  @Output()
  public readonly successs: EventEmitter<boolean> = new EventEmitter<boolean>();

  public pagination = {
    page: 1,
    pageSize: 10,
    totalElements: 13
  };

  constructor(private readonly departmentService: DepartmentApiService, private readonly modalService: NgbModal) { }

  ngOnInit() {
    this.loadDepartments();
  }

  public setRecordsPerPage(range?: any): void {
    this.pagination.pageSize = range;
    this.loadDepartments();
  }

  public setPage(page: number): void {
    this.pagination.page = page;
    this.loadDepartments();
  }

  private loadDepartments(): void {
    let query = {};
    if (this.filterBy && this.filterBy !== '') {
      query = { filter: this.filterBy };
    }
    this.departmentService.getAll(query).subscribe(response => {
      this.departmentList = response.records;
    }, error => { console.log('Se produjo un error al obtener departamentos ' + error); });
  }

  public openModal(isEdit: boolean = true, datos?: any): void {
    const modalRef = this.modalService.open(DepartmentModalComponent);
    modalRef.componentInstance.isEdit = isEdit;

    if (isEdit) {
      modalRef.componentInstance.department = datos;
    }

    modalRef.componentInstance.successs.subscribe(() => {
      this.loadDepartments();
    });

    modalRef.componentInstance.createdDepartment.subscribe(data => {
      this.departmentService.add(data).subscribe(() => {
        this.loadDepartments();
        console.log('success', data);
        modalRef.dismiss();
      });
    });
  }

  public deleteDepartment(id: number): void {
    Swal.fire({
      title: '¿Desea eliminar el registro seleccionado?',
      text: 'No será posible recuperarlo posteriormente!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Sí, eliminar!',
      cancelButtonText: 'No, cancelar'
    }).then((result) => {
      if (result.value) {
        this.departmentService.delete(id).subscribe(() => {
          this.loadDepartments();
        });
        Swal.fire(
          'Eliminado!',
          'Su registro ha sido eliminado',
          'success'
        )
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelado',
          'Su registro está seguro :)',
          'error'
        );
      }
    });
  }

  public searchs(): void {
    // vm.pageNum = 1;
    this.loadDepartments();
  }
}
