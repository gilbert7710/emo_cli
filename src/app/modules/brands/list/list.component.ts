import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BrandApiService } from 'src/app/core/data-services/brand-api.service';
import { BrandModalComponent } from '../brand-modal/brand-modal.component';
import Swal from 'sweetalert2';

@Component({
  selector: 'emo-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  public brandList: any[];

  @Input()
  public filterBy: any;
  @Input()
  public brand: any;
  @Output()
  public readonly successs: EventEmitter<boolean> = new EventEmitter<boolean>();

  public pagination = {
    page: 1,
    pageSize: 10,
    totalElements: 13
  };

  constructor(private readonly brandService: BrandApiService, private readonly modalService: NgbModal) { }

  ngOnInit() {
    this.loadBrands();
  }

  public setRecordsPerPage(range?: any): void {
    this.pagination.pageSize = range;
    this.loadBrands();
  }

  public setPage(page: number): void {
    this.pagination.page = page;
    this.loadBrands();
  }

  private loadBrands(): void {
    let query = {};
    if (this.filterBy && this.filterBy !== '') {
      query = { filter: this.filterBy };
    }
    this.brandService.getAll(query).subscribe(response => {
      this.brandList = response.records;
    }, error => { console.log('Se produjo un error al obtener marcas ' + error); });
  }

  public openModal(isEdit: boolean = true, datos?: any): void {
    const modalRef = this.modalService.open(BrandModalComponent);
    modalRef.componentInstance.isEdit = isEdit;

    if (isEdit) {
      modalRef.componentInstance.brand = datos;
    }

    modalRef.componentInstance.successs.subscribe(() => {
      this.loadBrands();
    });

    modalRef.componentInstance.createdBrand.subscribe(data => {
      this.brandService.add(data).subscribe(() => {
        this.loadBrands();
        console.log('success', data);
        modalRef.dismiss();
      });
    });
  }

  public deleteBrand(id: number): void {
    Swal.fire({
      title: '¿Desea eliminar el registro seleccionado?',
      text: 'No será posible recuperarlo posteriormente!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Sí, eliminar!',
      cancelButtonText: 'No, cancelar'
    }).then((result) => {
      if (result.value) {
        this.brandService.delete(id).subscribe(() => {
          this.loadBrands();
        });
        Swal.fire(
          'Eliminado!',
          'Su registro ha sido eliminado',
          'success'
        )
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelado',
          'Su registro está seguro :)',
          'error'
        );
      }
    });
  }

  public searchs(): void {
    // vm.pageNum = 1;
    this.loadBrands();
  }

}
