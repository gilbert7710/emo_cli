import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BrandsRoutingModule } from './brands-routing.module';
import { ParentComponent } from './parent/parent.component';
import { ListComponent } from './list/list.component';
import { BrandModalComponent } from './brand-modal/brand-modal.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [ParentComponent, ListComponent, BrandModalComponent],
  imports: [
    CommonModule,
    BrandsRoutingModule,
    SharedModule,
    NgbModule
  ],
  entryComponents: [BrandModalComponent]
})
export class BrandsModule { }
