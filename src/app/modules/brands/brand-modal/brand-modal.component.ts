import { BrandApiService } from 'src/app/core/data-services/brand-api.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'emo-brand-modal',
  templateUrl: './brand-modal.component.html',
  styleUrls: ['./brand-modal.component.scss']
})
export class BrandModalComponent implements OnInit {

  @Input()
  public isEdit: boolean;
  @Input()
  public brand: any;
  @Output()
  public readonly successs: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output()
  public createdBrand: EventEmitter<any> = new EventEmitter<any>();
  @Output()
  public editedBrand: EventEmitter<boolean> = new EventEmitter<boolean>();

  public form: FormGroup;

  constructor(
    private readonly fb: FormBuilder,
    private readonly brandService: BrandApiService,
    private readonly modalInstance: NgbActiveModal) { }

  ngOnInit() {
    this.initForm();
  }

  private initForm(): void {
    this.form = this.fb.group({
      code: ['', Validators.required],
      desc: ['', Validators.required],
      status: true
    });

    if (this.isEdit) {
      this.form.patchValue(this.brand);
    }
  }

  public onSubmit(): void {
    if (this.isEdit) {
      this.brandService.update(this.brand.id, this.form.value).subscribe(
        () => {
          this.successs.emit(true);
          this.dismiss();
        },
        error => {
          console.log(error);
        }
      );
    } else {
      // const payload: any = {};
      this.createdBrand.emit(this.form.value);
    }
  }

  public dismiss(): void {
    this.modalInstance.dismiss();
  }

}
