import { CustomerModalComponent } from './../customer-modal/customer-modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { CustomerApiService } from 'src/app/core/data-services/customer-api.service';

@Component({
  selector: 'emo-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  public customersList: any[];

  @Input()
  public customer: any;
  @Output()
  public readonly successs: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private readonly customerService: CustomerApiService, private readonly modalService: NgbModal) { }

  ngOnInit() {
    this.loadCustomers();
  }

  private loadCustomers(): void {
    this.customerService.getAll().subscribe(response => {
      this.customersList = response.records;
    }, error => { console.log('Se produjo un error al obtener productos ' + error); });
  }

  public openCustomerModal(isEdit: boolean = true): void {
    const modalRef = this.modalService.open(CustomerModalComponent);
    modalRef.componentInstance.isEdit = isEdit;
    modalRef.componentInstance.successs.subscribe(() => {
      this.loadCustomers();
    });
    modalRef.componentInstance.createdCustomer.subscribe(data => {
      this.customerService.add(data).subscribe(() => {
        this.loadCustomers();
        console.log('success', data);
        modalRef.dismiss();
      });
    });
  }

  // public editCustomer(id: number): void {
  //   const modalRef = this.modalService.open(CustomerModalComponent);
  //   modalRef.componentInstance.isEdit = true;
  //   modalRef.componentInstance.customer = this.customer;
  //   modalRef.componentInstance.editedProduct.subscribe(() => {
  //     this.editedCustomer.emit(true);
  //     modalRef.dismiss();
  //   });
  // }

  // public Customer(id: number): void {
  //   this.customerService.delete(id).subscribe(() => {
  //     this.loadCustomers();
  //   });
  // }

}
