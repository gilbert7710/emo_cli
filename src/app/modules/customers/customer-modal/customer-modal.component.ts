import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { CustomerApiService } from 'src/app/core/data-services/customer-api.service';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'emo-customer-modal',
  templateUrl: './customer-modal.component.html',
  styleUrls: ['./customer-modal.component.scss']
})
export class CustomerModalComponent implements OnInit {
  @Input()
  public isEdit: boolean;
  @Input()
  public customer: any;
  @Output()
  public readonly successs: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output()
  public createdCustomer: EventEmitter<any> = new EventEmitter<any>();
  @Output()
  public editedCustomer: EventEmitter<boolean> = new EventEmitter<boolean>();

  public form: FormGroup;

  constructor(
    private readonly fb: FormBuilder,
    private readonly customerService: CustomerApiService,
    private readonly modalInstance: NgbActiveModal) { }

  ngOnInit() {
    this.initForm();
  }

  private initForm(): void {
    this.form = this.fb.group({
      name: ['', Validators.required],
      age: ['', Validators.required],
      description: ['', Validators.required]
    });

    if (this.isEdit) {
      this.form.patchValue(this.customer);
    }
  }

  public onSubmit(): void {
    if (this.isEdit) {
      this.customerService
        .update(this.customer.id, this.form.value)
        .subscribe(
          () => {
            this.successs.emit(true);
            this.dismiss();
          },
          error => {
            console.log(error);
          }
        );
    } else {
      const payload: any = {};
      this.createdCustomer.emit(payload);
    }
  }

  public dismiss(): void {
    this.modalInstance.dismiss();
  }

}
