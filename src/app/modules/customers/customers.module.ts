import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { CustomersRoutingModule } from './customers-routing.module';
import { ParentComponent } from './parent/parent.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { ListComponent } from './list/list.component';
import { CustomerModalComponent } from './customer-modal/customer-modal.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [ParentComponent, ListComponent, CustomerModalComponent],
  imports: [
    CustomersRoutingModule,
    SharedModule,
    CommonModule,
    NgbModule
  ],
  entryComponents: [CustomerModalComponent]
})
export class CustomersModule { }
