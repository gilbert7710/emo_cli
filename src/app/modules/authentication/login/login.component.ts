import { Component, OnInit } from '@angular/core';
import { CONFIG } from 'src/app/config';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'emo-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public isLoggedIn = false;
  public emailPattern = CONFIG.patterns.email;

  constructor(
    private readonly route: Router,
    private readonly authenticationService: AuthenticationService) { }

  ngOnInit() {
  }

  public login(form: NgForm): void {

    if (this.authenticationService.login(form.value)) {
      this.route.navigate(['/departments']);
    }
  }

  public goToSingUp() {
    this.route.navigate(['/sign-up']);
  }

}
