import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { WarehouseApiService } from 'src/app/core/data-services/warehouse-api.service';

@Component({
  selector: 'emo-warehouse-modal',
  templateUrl: './warehouse-modal.component.html',
  styleUrls: ['./warehouse-modal.component.scss']
})
export class WarehouseModalComponent implements OnInit {

  @Input()
  public isEdit: boolean;
  @Input()
  public warehouse: any;
  @Output()
  public readonly successs: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output()
  public createdWarehouse: EventEmitter<any> = new EventEmitter<any>();
  @Output()
  public editedWarehouse: EventEmitter<boolean> = new EventEmitter<boolean>();

  public form: FormGroup;

  constructor(
    private readonly fb: FormBuilder,
    private readonly warehouseService: WarehouseApiService,
    private readonly modalInstance: NgbActiveModal) { }

  ngOnInit() {
    this.initForm();
  }

  private initForm(): void {
    this.form = this.fb.group({
      code: ['', Validators.required],
      name: ['', Validators.required],
      phone1: ['', Validators.required],
      phone2: ['', Validators.required],
      address: ['', Validators.required],
      status: true
    });

    if (this.isEdit) {
      this.form.patchValue(this.warehouse);
    }
  }

  public onSubmit(): void {
    if (this.isEdit) {
      this.warehouseService
        .update(this.warehouse.id, this.form.value)
        .subscribe(
          () => {
            this.successs.emit(true);
            this.dismiss();
          },
          error => {
            console.log(error);
          }
        );
    } else {
      // const payload: any = {};
      this.createdWarehouse.emit(this.form.value);
    }
  }

  public dismiss(): void {
    this.modalInstance.dismiss();
  }

}
