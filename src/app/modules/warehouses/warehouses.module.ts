import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WarehousesRoutingModule } from './warehouses-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { WarehouseModalComponent } from './warehouse-modal/warehouse-modal.component';
import { ParentComponent } from './parent/parent.component';
import { ListComponent } from './list/list.component';


@NgModule({
  declarations: [ParentComponent, ListComponent, WarehouseModalComponent],
  imports: [
    CommonModule,
    WarehousesRoutingModule,
    SharedModule,
    NgbModule
  ],
  entryComponents: [WarehouseModalComponent]
})
export class WarehousesModule { }
